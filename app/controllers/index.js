const filterController = require('./Filter/filterController');
const testController = require('./testController');
const autobuyerItemsTradeStatisticController = require('./Statistic/autobuyerItemsTradeStatisticController');
const autobuyerRequestsStatisticController = require('./Statistic/autobuyerRequestsStatisticController');
const autobuyerPlayersTransferListStatisticController = require('./Statistic/autobuyerPlayersTransferListStatisticController');

module.exports = {
    filterController,
    testController,
    autobuyerItemsTradeStatisticController,
    autobuyerRequestsStatisticController,
    autobuyerPlayersTransferListStatisticController
}