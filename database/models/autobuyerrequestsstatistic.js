'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class AutobuyerRequestsStatistic extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  AutobuyerRequestsStatistic.init({
    total_buyplayer_count: DataTypes.INTEGER,
    bid_count: DataTypes.INTEGER,
    buy_count: DataTypes.INTEGER,
    outbid_count: DataTypes.INTEGER,
    search_count: DataTypes.INTEGER,
    list_player_count: DataTypes.INTEGER,
    relist_player_count: DataTypes.INTEGER,
    remove_player_count: DataTypes.INTEGER,
  }, {
    sequelize,
    modelName: 'AutobuyerRequestsStatistic',
  });
  return AutobuyerRequestsStatistic;
};