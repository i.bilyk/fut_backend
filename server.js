require('dotenv').config()

const express = require('express'),
    cors = require('cors'),
    app = express(),
    port = process.env.PORT || 9000,
    bodyParser = require('body-parser'),
    logger = require('morgan');

app.use(cors());
app.use(logger('dev'))
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use('/api', require('./routes/api'));

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})