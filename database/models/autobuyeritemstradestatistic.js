'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class AutobuyerItemsTradeStatistic extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  AutobuyerItemsTradeStatistic.init({
    sttl_count: DataTypes.INTEGER,
    llmb_count: DataTypes.INTEGER,
    ltmb_count: DataTypes.INTEGER,
    estimated_profit: DataTypes.FLOAT
  }, {
    sequelize,
    modelName: 'AutobuyerItemsTradeStatistic',
  });
  return AutobuyerItemsTradeStatistic;
};