'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('AutobuyerItemsTradeStatistics', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      sttl_count: {
        type: Sequelize.INTEGER
      },
      llmb_count: {
        type: Sequelize.INTEGER
      },
      ltmb_count: {
        type: Sequelize.INTEGER
      },
      estimated_profit: {
        type: Sequelize.FLOAT
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('AutobuyerItemsTradeStatistics');
  }
};