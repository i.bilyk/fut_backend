'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('AutobuyerPlayersTransferListStatistics', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.STRING
      },
      futbin_price: {
        type: Sequelize.INTEGER
      },
      buy_now_price: {
        type: Sequelize.INTEGER
      },
      minimal_bid: {
        type: Sequelize.INTEGER
      },
      current_bid: {
        type: Sequelize.INTEGER
      },
      position: {
        type: Sequelize.STRING
      },
      rating: {
        type: Sequelize.INTEGER
      },
      trade_id: {
        type: Sequelize.BIGINT
      },
      trade_state: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('AutobuyerPlayersTransferListStatistics');
  }
};