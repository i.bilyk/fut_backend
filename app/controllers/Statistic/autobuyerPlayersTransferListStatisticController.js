const autobuyerPlayersTransferListStatisticService = require('./../../services/Statistic/autobuyerPlayersTransferListStatisticService')
const AutobuyerPlayersTransferListStatisticService = new autobuyerPlayersTransferListStatisticService();

const create = (req, res) => {
    AutobuyerPlayersTransferListStatisticService.create(req.body)
        .then(() => {
            return res.status(201).send({
                success: true,
                message: 'Recorded success!',
                data: []
            });
        }).catch(console.log)
}

module.exports = {create}