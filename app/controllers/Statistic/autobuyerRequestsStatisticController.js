const autobuyerRequestsStatisticService = require('./../../services/Statistic/autobuyerRequestsStatisticService')
const AutobuyerRequestsStatisticService = new autobuyerRequestsStatisticService();

const create = (req, res) => {
    AutobuyerRequestsStatisticService.create(req.body)
        .then(model => {
            return res.status(201).send({
                success: true,
                message: 'Recorded success!',
                data: []
            });
        }).catch(console.log)
}

module.exports = {create}