let Filter = require('../../../database/models').Filter;

module.exports = class filterService {
    create = async (requestBody) => {
        await Filter.findOne({where: {name: requestBody.name}}).then(filter => {
            if (filter) {
                return filter.update(requestBody);
            }

            return Filter.create(requestBody);
        }).catch(error => {
            console.log(error)
        })
    }

    getAll = async () => {
        return await Filter.findAll();
    }
}