const filterServiceClass = require('./../../services/Filter/filterService')
const filterServiceInstance = new filterServiceClass();

const create = (req, res) => {
    return filterServiceInstance.create(req.body)
        .then(filter => {
            return res.send({
                'success': true,
                'data': filter
            })
        }).catch(error => {
            return res.send({
                'success': false,
                'data': error
            })
        });
}

const update = (req, res) => {

}

const getAll = (req, res) => {
    filterServiceInstance.getAll().then(filters => {
        res.send({
            success: true,
            data: filters
        });
    }).catch(error => {
        res.send({
            success: false
        });
    });
}

module.exports = {create, update, getAll}