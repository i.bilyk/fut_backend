const app = require('express');
const router = app.Router();
const apiKeyMiddleware = require('../app/middlewares/apiKeyMiddleware');

const controllers = require('../app/controllers/index');

const filterController = controllers.filterController;
const autobuyerItemsTradeStatisticController = controllers.autobuyerItemsTradeStatisticController;
const autobuyerRequestsStatisticController = controllers.autobuyerRequestsStatisticController;
const autobuyerPlayersTransferListStatisticController = controllers.autobuyerPlayersTransferListStatisticController;
const testController = controllers.testController;

router.get('/ping', testController.pong)

router.use(apiKeyMiddleware)

//filters
router.post('/filters', filterController.create)
router.get('/filters', filterController.getAll)

//statistic
router.post('/autobuyer/statistic/trade-items', autobuyerItemsTradeStatisticController.create)
router.post('/autobuyer/statistic/requests', autobuyerRequestsStatisticController.create)
router.post('/autobuyer/statistic/players', autobuyerPlayersTransferListStatisticController.create)

module.exports = router;