let AutobuyerRequestsStatisticModel = require('../../../database/models').AutobuyerRequestsStatistic;

module.exports = class AutobuyerRequestsStatisticService {
    create = (requestBody) => {
        return AutobuyerRequestsStatisticModel.create(requestBody);
    }
}