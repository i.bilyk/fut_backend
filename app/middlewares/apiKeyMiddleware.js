const apiKeyMiddleware = async (req, res, next) => {
    const apiKey = req.query.apiKey;

    if (apiKey === undefined) {
        return res.status(401).send({
            success: false,
            message: 'API key required!',
        })
    }

    let ApiKey = require('../../database/models').ApiKey;

    return await ApiKey.findOne({where: {key: apiKey}}).then(filter => {
        if (!filter) {
            return res.status(401).send({
                success: false,
                message: 'Provided API key is not valid!',
            })
        }

        return next();
    }).catch(console.log);
}

module.exports = apiKeyMiddleware;