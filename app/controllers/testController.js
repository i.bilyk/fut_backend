const testService = require('./../services/testService')
const testServiceInstance = new testService();

const pong = (req, res) => {
    const testResult = testServiceInstance.test();

    return res.send(testResult);
}

module.exports = {pong}