'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class AutobuyerPlayersTransferListStatistic extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  AutobuyerPlayersTransferListStatistic.init({
    name: DataTypes.STRING,
    futbin_price: DataTypes.INTEGER,
    buy_now_price: DataTypes.INTEGER,
    minimal_bid: DataTypes.INTEGER,
    current_bid: DataTypes.INTEGER,
    position: DataTypes.STRING,
    rating: DataTypes.INTEGER,
    trade_id: DataTypes.BIGINT,
    trade_state: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'AutobuyerPlayersTransferListStatistic',
  });
  return AutobuyerPlayersTransferListStatistic;
};