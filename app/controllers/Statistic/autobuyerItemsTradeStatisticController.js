const autobuyerItemsTradeStatisticService = require('./../../services/Statistic/autobuyerItemsTradeStatisticService')
const AutobuyerItemsTradeStatisticService = new autobuyerItemsTradeStatisticService();

const create = (req, res) => {
    AutobuyerItemsTradeStatisticService.create(req.body)
        .then(model => {
            return res.status(201).send({
                success: true,
                message: 'Recorded success!',
                data: []
            });
        }).catch(console.log)
}

module.exports = {create}