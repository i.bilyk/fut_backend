'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('AutobuyerRequestsStatistics', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      total_buyplayer_count: {
        type: Sequelize.INTEGER
      },
      bid_count: {
        type: Sequelize.INTEGER
      },
      buy_count: {
        type: Sequelize.INTEGER
      },
      outbid_count: {
        type: Sequelize.INTEGER
      },
      search_count: {
        type: Sequelize.INTEGER
      },
      list_player_count: {
        type: Sequelize.INTEGER
      },
      relist_player_count: {
        type: Sequelize.INTEGER
      },
      remove_player_count: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('AutobuyerRequestsStatistics');
  }
};