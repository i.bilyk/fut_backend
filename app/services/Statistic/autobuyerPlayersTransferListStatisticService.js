let AutobuyerPlayersTransferListStatisticModel = require('../../../database/models').AutobuyerPlayersTransferListStatistic;

module.exports = class AutobuyerPlayersTransferListStatisticService {
    create = (requestBody) => {
        return AutobuyerPlayersTransferListStatisticModel.bulkCreate(requestBody.data);
    }
}