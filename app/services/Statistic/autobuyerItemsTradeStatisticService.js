let AutobuyerItemsTradeStatisticModel = require('../../../database/models').AutobuyerItemsTradeStatistic;

module.exports = class AutobuyerItemsTradeStatisticService {
    create = (requestBody) => {
        return AutobuyerItemsTradeStatisticModel.create(requestBody);
    }
}